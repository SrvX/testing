@extends('baselayout')

@section('content')

<div class="row">
	<h1>Профиль джентельмена удачи по имени <br/>{{ $pirate->name }}</h1>
	<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
      </div>
	<div class="container">
	    <div class="row">
	        <div class="col-xs-3 pirate--avatar">
	            <img class="pirate--image" src="{{ $pirate->avatar }}" alt=""/>
	            <p>Пиастров и дублонов: {{ $pirate->money }}</p>
	        </div>
	        <div class="col-xs-6">
	            @if ($pirate->booties->count())
                    <h3>Нажитое непосильным трудом</h3>
                    <table class="table table-striped owned-table">
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Цена</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pirate->booties as $booty)
                                <tr>
                                    <td>{{ $booty->name }}</td>
                                    <td>{{ number_format($booty->price, 0, '', ' ') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <h3>Продать сокровище</h3>
                      @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                    {!! Form::open(['class' => 'sell_treasure-form', 'method' => 'put']) !!}
                        <div class="form-group">
                            <label class="col-xs-4">Кому продаём?</label>
                            <div class="col-xs-8">
                                @foreach($other_pirates as $other_pirate)
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="{{ $other_pirate->id }}" name="to"/>
                                            {{ $other_pirate->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4">Что продаём-то?</label>
                            <div class="col-xs-8">
                                @foreach ($pirate->booties as $booty)
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="{{ $booty->id }}" name="booty"/>
                                            {{ $booty->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <input type="submit" class="col-xs-offset-4 btn btn-default" value="Аррр! Деньги на бочку!"/>
                    {!! Form::close() !!}
                @else
                    <p>По сокровищам гол как сокол.</p>
                @endif

	        </div>
	    </div>
	</div>
</div>

@stop
	