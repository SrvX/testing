@extends('baselayout')

@section('content')

<div class="row">
	<h1>Наши достопочтенные господа</h1>
	<div class="container">
        @foreach ($pirates->chunk(4) as $items)
            <div class="row">
                @foreach ($items as $pirate)
                    <div class="col-xs-3 pirate--avatar">
                        <div class="pirate--image_wrapper">
                            <a href="{{ action('PiratesController@show', $pirate) }}">
                                <img class="pirate--image" src="{{ $pirate->avatar }}" alt=""/>
                            </a>
                        </div>
                        <h3>{{ $pirate->name }}</h3>
                    </div>
                @endforeach
            </div>
        @endforeach
	</div>
</div>

@stop
	