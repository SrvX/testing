<?php

use App\Models\Booty;
use App\Models\Pirate;
use App\Models\User;
use Illuminate\Database\Seeder;

class CreatePiratesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     function run()
    {
        DB::table('users')->delete();
        DB::table('booties')->delete();
        $evil_jack = User::create([
            'name' => 'evil_jack',
            'email' => 'evil_jack@findevelop.ru'
        ]);
        $evil_jack_pirate = Pirate::create([
            'name' => 'Злобный Джек',
            'user_id' => $evil_jack->id,
            'money' => 100,
            'avatar' => '/images/Pirate1.png'
        ]);


        $wild_bill = User::create([
            'name' => 'wild_bill',
            'email' => 'wild_bill@findevelop.ru'
        ]);
        $wild_bill_pirate = Pirate::create([
            'name' => 'Дикий Билл',
            'user_id' => $wild_bill->id,
            'money' => 900,
            'avatar' => '/images/Pirate2.jpg'
        ]);


        $deadman_chris = User::create([
            'name' => 'deadman',
            'email' => 'deadman@findevelop.ru'
        ]);
        $deadman_chris_pirate = Pirate::create([
            'name' => 'Кристофер «Мертвец» Джонсон',
            'user_id' => $deadman_chris->id,
            'money' => -200,
            'avatar' => '/images/Pirate4.png'
        ]);


        $richman = User::create([
            'name' => 'richman',
            'email' => 'richman@findevelop.ru'
        ]);
        $richman_pirate = Pirate::create([
            'name' => 'Александр «Богач» Доусон',
            'user_id' => $richman->id,
            'money' => 0,
            'avatar' => '/images/Pirate5.jpg'
        ]);


        $oldman = User::create([
            'name' => 'oldman',
            'email' => 'oldman@findevelop.ru'
        ]);
        $oldman_pirate = Pirate::create([
            'name' => 'Престарелый Бак',
            'user_id' => $oldman->id,
            'money' => 10000,
            'avatar' => '/images/Pirate3.jpg'
        ]);

        Booty::create([
            'name' => 'Лапа дракона',
            'price' => 1000,
            'owner_id' => $evil_jack_pirate->id
        ]);
        Booty::create([
            'name' => 'Маховик времени',
            'price' => 9000,
            'owner_id' => $deadman_chris_pirate->id
        ]);
        Booty::create([
            'name' => 'Чья-то прелесть',
            'price' => 300,
            'owner_id' => $deadman_chris_pirate->id
        ]);
        Booty::create([
            'name' => 'Шкура неубитого медведя',
            'price' => 500,
            'owner_id' => $wild_bill_pirate->id
        ]);
        Booty::create([
            'name' => 'Долги руководителя колл-центра',
            'price' => 777,
            'owner_id' => $richman_pirate->id
        ]);
        Booty::create([
            'name' => 'Место программиста в «Займере»',
            'price' => 999,
            'owner_id' => $evil_jack_pirate->id
        ]);
        Booty::create([
            'name' => 'Диплом',
            'price' => 5,
            'owner_id' => $deadman_chris_pirate->id
        ]);
        Booty::create([
            'name' => 'Свинья, сворованная у Сваровски',
            'price' => 1000,
            'owner_id' => $oldman_pirate->id
        ]);
        Booty::create([
            'name' => 'Сундук мертвеца',
            'price' => 300,
            'owner_id' => $deadman_chris_pirate->id
        ]);
        Booty::create([
            'name' => 'Йо-хо-хо и бутылка Рома',
            'price' => 300,
            'owner_id' => $oldman_pirate->id
        ]);
        Booty::create([
            'name' => 'Неподписанная видеокасета 1997г. с чьим-то выпускным',
            'price' => 15,
            'owner_id' => $oldman_pirate->id
        ]);
        Booty::create([
            'name' => 'Правая рука Хэллбоя',
            'price' => 99999,
            'owner_id' => $deadman_chris_pirate->id
        ]);
        Booty::create([
            'name' => 'Билет на концерт Тимати',
            'price' => 1,
            'owner_id' => $wild_bill_pirate->id
        ]);
        Booty::create([
            'name' => 'Футбольный мяч с подписью Александра Олежки',
            'price' => 18,
            'owner_id' => $wild_bill_pirate->id
        ]);
        Booty::create([
            'name' => 'Титул наследника сэра Фрэнсиса Дрейка',
            'price' => 500,
            'owner_id' => $richman_pirate->id
        ]);

    }
}
