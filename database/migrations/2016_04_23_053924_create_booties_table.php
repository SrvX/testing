<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBootiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->float('price')->default(0);
            $table->unsignedInteger('owner_id')->nullable();
            $table->timestamps();
        });

        Schema::table('booties', function(Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booties');
    }
}
