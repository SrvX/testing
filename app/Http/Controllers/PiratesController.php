<?php

namespace App\Http\Controllers;

use App\Exceptions\PirateException;
use App\Models\Booty;
use App\Models\Pirate;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class PiratesController extends Controller
{
    public function show($pirate_id){
        $pirate = Pirate::find($pirate_id);

        return view('pirate', [
            'pirate' => $pirate,
            'other_pirates' => Pirate::where('id', '<>', $pirate->id)->get()
        ]);
    }

    public function update($pirate_id, Request $request){
        $this->validate($request, [
            'to' => 'required',
            'booty' => 'required'
        ]);
        /** @var Pirate $pirate */
        $pirate = Pirate::find($pirate_id);
        try {
            $booty_id = Input::get('booty');
            /** @var Booty $booty */
            $booty = Booty::find($booty_id);
            // Проверяем есть ли у нас такое сокровище
            if(!$booty) {
                throw new PirateException('Not our treasure, mate!');
            }

            // Проверяем принадлежит ли сокровище пирату
            if(!$pirate->booties()->where('id', $booty_id)->first()) {
                throw new PirateException('This pirate doesn\'t have that treasure!');
            }

            $to = Input::get('to');
            /** @var Pirate $pirate_to */
            $pirate_to = Pirate::find($to);
            // Проверяем есть ли у нас пират, которому переводят сокровище
            if(!$pirate_to) {
                throw new PirateException('We don\'t know who that is, yo!');
            }

            // Производим передачу сокровища взамен на деньги
            DB::beginTransaction();

            $pirate_to->money -= $booty->price;
            $pirate_to->booties()->save($booty);
            $pirate->money += $booty->price;
            $pirate->save();
            $pirate_to->save();

            DB::commit();

            // Сообщение об успешной отправке
            $request->session()->flash('alert-success', 'Success! You have become rich indeed
                (+ ' . number_format($booty->price, 0, '', ' ') . ' gold)');
        } catch(PirateException $e) {
            $request->session()->flash('alert-danger', $e->getMessage());
        }

        return redirect(action('PiratesController@show', ['pirate' => $pirate]));
    }
}
