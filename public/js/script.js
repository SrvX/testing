$(window).on('load', function() {
    var maxHeight = 0;

    $('.pirate--avatar').each(function() {
        if($(this).find('.pirate--image').outerHeight() > maxHeight) {
            maxHeight = $(this).find('.pirate--image').outerHeight();
        }
    });
    $('.pirate--image_wrapper').css({'height': maxHeight});
    $('.pirate--avatar').each(function() {
        var $image = $(this).find('.pirate--image');
        $image.css({
            'marginTop': maxHeight - $image.outerHeight()
        });
    });
});